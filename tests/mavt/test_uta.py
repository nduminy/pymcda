import unittest

from pandas import Series
from pulp import LpMinimize

from mcda.functions import Interval
from mcda.matrices import PerformanceTable
from mcda.mavt.uta import UTA, UTAstar
from mcda.relations import I, P, PreferenceStructure, R
from mcda.scales import (
    DiscreteQuantitativeScale,
    PreferenceDirection,
    QualitativeScale,
    QuantitativeScale,
)
from mcda.set_functions import HashableSet
from mcda.transformers import Transformer


class UTATestCase(unittest.TestCase):
    def setUp(self):
        self.alternatives = [
            "Peugeot 505 GR",
            "Opel Record 2000 LS",
            "Citroen Visa Super E",
            "VW Golf 1300 GLS",
            "Citroen CX 2400 Pallas",
            "Mercedes 230",
            "BMW 520",
            "Volvo 244 DL",
            "Peugeot 104 ZS",
            "Citroen Dyane",
        ]
        self.scales = {
            0: QuantitativeScale(Interval(110, 190)),
            1: QuantitativeScale(
                Interval(7, 15), preference_direction=PreferenceDirection.MIN
            ),
            2: QuantitativeScale(
                Interval(6, 13), preference_direction=PreferenceDirection.MIN
            ),
            3: QuantitativeScale(Interval(3, 13)),
            4: QuantitativeScale(Interval(5, 9)),
            5: QuantitativeScale(
                Interval(20000, 80000),
                preference_direction=PreferenceDirection.MIN,
            ),
        }
        self.performance_table = PerformanceTable(
            [
                [173, 11.4, 10.01, 10, 7.88, 49500],
                [176, 12.3, 10.48, 11, 7.96, 46700],
                [142, 8.2, 7.3, 5, 5.65, 32100],
                [148, 10.5, 9.61, 7, 6.15, 39150],
                [178, 14.5, 11.05, 13, 8.06, 64700],
                [180, 13.6, 10.4, 13, 8.47, 75700],
                [182, 12.7, 12.26, 11, 7.81, 68593],
                [145, 14.3, 12.95, 11, 8.38, 55000],
                [161, 8.6, 8.42, 7, 5.11, 35200],
                [117, 7.2, 6.75, 3, 5.81, 24800],
            ],
            alternatives=self.alternatives,
            scales=self.scales,
        )
        self.criteria_segments = {0: 5, 1: 4, 2: 4, 3: 5, 4: 4, 5: 5}
        self.relations = PreferenceStructure()
        for i in range(len(self.alternatives) - 1):
            self.relations += P(self.alternatives[i], self.alternatives[i + 1])
        self.normalized_table = Transformer.normalize(self.performance_table)
        self.uta = UTA(
            self.performance_table,
            self.relations,
            self.criteria_segments,
            delta=0.01,
        )

    def test_constructor(self):
        self.assertEqual(self.uta.performance_table, self.performance_table)
        self.assertEqual(self.uta.preference_structure, self.relations)
        self.assertEqual(self.uta.delta, 0.01)
        self.assertIsNotNone(self.uta.post_optimality_problem)
        self.assertIsNotNone(self.uta.problem)
        self.assertEqual(self.uta.problem.sense, LpMinimize)

        uta = UTA(self.performance_table, self.relations)
        self.assertEqual(
            uta.criteria_segments,
            {c: 2 for c in self.performance_table.criteria},
        )

        relations = self.relations.copy()
        relations -= relations.elements_pairs_relations[
            HashableSet([self.alternatives[0], self.alternatives[1]])
        ]
        relations += R(self.alternatives[0], self.alternatives[1])
        self.assertRaises(TypeError, UTA, self.performance_table, relations)

    def test_learn(self):
        functions = self.uta.learn()
        grades = functions.aggregate(functions(self.performance_table))
        ranks = grades.sort()
        self.assertEqual(grades.labels, ranks.labels)
        self.assertEqual(self.uta.objective, 0)

        uta = UTA(
            self.performance_table,
            self.relations,
            self.criteria_segments,
            delta=0.01,
            post_optimality=True,
        )
        functions = uta.disaggregate()
        grades = functions.aggregate(functions(self.performance_table))
        ranks = grades.sort()
        self.assertEqual(grades.labels, ranks.labels)
        self.assertAlmostEqual(uta.objective, 0)

        relations = self.relations.copy()
        relations -= relations.elements_pairs_relations[
            HashableSet((self.alternatives[2], self.alternatives[3]))
        ]
        relations += I(
            self.alternatives[2],
            self.alternatives[3],
        )
        uta = UTA(
            self.performance_table,
            relations,
            self.criteria_segments,
            delta=0.01,
            post_optimality=True,
        )
        functions = uta.disaggregate()
        grades = functions.aggregate(functions(self.performance_table))
        ranks = grades.sort()
        self.assertEqual(grades.labels, ranks.labels)
        self.assertAlmostEqual(uta.objective, 0)

    def test_learn_smaller(self):
        table = PerformanceTable(
            [
                ["Good", 2, 150],
                ["Perfect", 3, 50],
                ["Bad", 1, 300],
                ["Good", 1, 200],
            ],
            {
                0: QualitativeScale(
                    Series({"Bad": 3, "Good": 2, "Perfect": 1}),
                    PreferenceDirection.MIN,
                ),
                1: DiscreteQuantitativeScale([1, 2, 3]),
                2: QuantitativeScale(
                    0, 500, preference_direction=PreferenceDirection.MIN
                ),
            },
        )
        relations = PreferenceStructure([P(1, 0), P(0, 2)])
        uta = UTA(table, relations)
        functions = uta.learn()
        ranking = functions.aggregate(functions(table))
        res_prefs = PreferenceStructure.from_ranking(ranking)
        self.assertTrue(
            set(relations.relations).issubset(set(res_prefs.relations))
        )

    def test_rank(self):
        grades = self.uta.rank()
        ranks = grades.sort()
        self.assertEqual(grades.labels, ranks.labels)


class UTAstarTestCase(unittest.TestCase):
    def setUp(self):
        self.alternatives = [
            "Peugeot 505 GR",
            "Opel Record 2000 LS",
            "Citroen Visa Super E",
            "VW Golf 1300 GLS",
            "Citroen CX 2400 Pallas",
            "Mercedes 230",
            "BMW 520",
            "Volvo 244 DL",
            "Peugeot 104 ZS",
            "Citroen Dyane",
        ]
        self.scales = {
            0: QuantitativeScale(Interval(110, 190)),
            1: QuantitativeScale(
                Interval(7, 15), preference_direction=PreferenceDirection.MIN
            ),
            2: QuantitativeScale(
                Interval(6, 13), preference_direction=PreferenceDirection.MIN
            ),
            3: QuantitativeScale(Interval(3, 13)),
            4: QuantitativeScale(Interval(5, 9)),
            5: QuantitativeScale(
                Interval(20000, 80000),
                preference_direction=PreferenceDirection.MIN,
            ),
        }
        self.performance_table = PerformanceTable(
            [
                [173, 11.4, 10.01, 10, 7.88, 49500],
                [176, 12.3, 10.48, 11, 7.96, 46700],
                [142, 8.2, 7.3, 5, 5.65, 32100],
                [148, 10.5, 9.61, 7, 6.15, 39150],
                [178, 14.5, 11.05, 13, 8.06, 64700],
                [180, 13.6, 10.4, 13, 8.47, 75700],
                [182, 12.7, 12.26, 11, 7.81, 68593],
                [145, 14.3, 12.95, 11, 8.38, 55000],
                [161, 8.6, 8.42, 7, 5.11, 35200],
                [117, 7.2, 6.75, 3, 5.81, 24800],
            ],
            alternatives=self.alternatives,
            scales=self.scales,
        )
        self.criteria_segments = {0: 5, 1: 4, 2: 4, 3: 5, 4: 4, 5: 5}
        self.relations = PreferenceStructure()
        for i in range(len(self.alternatives) - 1):
            self.relations += P(self.alternatives[i], self.alternatives[i + 1])
        self.normalized_table = Transformer.normalize(self.performance_table)
        self.uta_star = UTAstar(
            self.performance_table,
            self.relations,
            self.criteria_segments,
            delta=0.01,
        )

    def test_constructor(self):
        self.assertEqual(self.uta_star.problem.sense, LpMinimize)
        self.assertIsNotNone(self.uta_star.post_optimality_problem)

    def test_disaggregate(self):
        functions = self.uta_star.learn()
        grades = functions.aggregate(functions(self.performance_table))
        ranks = grades.sort()
        self.assertEqual(grades.labels, ranks.labels)
        self.assertEqual(self.uta_star.objective, 0)

        uta_star = UTAstar(
            self.performance_table,
            self.relations,
            self.criteria_segments,
            delta=0.01,
            post_optimality=True,
        )
        functions = uta_star.disaggregate()
        grades = functions.aggregate(functions(self.performance_table))
        ranks = grades.sort()
        self.assertEqual(grades.labels, ranks.labels)
        self.assertAlmostEqual(uta_star.objective, 0)

        relations = self.relations.copy()
        relations -= relations.elements_pairs_relations[
            HashableSet((self.alternatives[2], self.alternatives[3]))
        ]
        relations += I(
            self.alternatives[2],
            self.alternatives[3],
        )
        uta_star = UTAstar(
            self.performance_table,
            relations,
            self.criteria_segments,
            delta=0.01,
            post_optimality=True,
        )
        functions = uta_star.disaggregate()
        grades = functions.aggregate(functions(self.performance_table))
        ranks = grades.sort()
        self.assertEqual(grades.labels, ranks.labels)
        self.assertAlmostEqual(uta_star.objective, 0)

    def test_rank(self):
        grades = self.uta_star.rank()
        ranks = grades.sort()
        self.assertEqual(grades.labels, ranks.labels)
