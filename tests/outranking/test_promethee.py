import unittest

from pandas import DataFrame, Series
from pandas.testing import assert_frame_equal, assert_series_equal

from mcda.functions import (
    GaussianFunction,
    Interval,
    LevelFunction,
    UShapeFunction,
    VShapeFunction,
)
from mcda.internal.outranking.promethee import (
    GeneralizedCriteria,
    GeneralizedCriterion,
)
from mcda.matrices import (
    AdjacencyValueMatrix,
    PartialValueMatrix,
    PerformanceTable,
)
from mcda.outranking.promethee import (
    Promethee1,
    Promethee2,
    PrometheeGaia,
    criteria_flows,
    negative_flows,
    net_outranking_flows,
    positive_flows,
)
from mcda.relations import I, P, PreferenceStructure, R
from mcda.scales import PreferenceDirection, QuantitativeScale
from mcda.values import CommensurableValues, Values


class TestGeneralizedCriterion(unittest.TestCase):
    def setUp(self):
        self.scale = QuantitativeScale(
            Interval(0, 10), preference_direction=PreferenceDirection.MAX
        )
        self.function = GeneralizedCriterion(UShapeFunction(), self.scale)
        self.values = CommensurableValues([0, 1], scale=self.scale)

    def test_within_scale(self):
        self.assertTrue(self.function.within_scale(self.values))
        self.assertTrue(self.function.within_scale(self.values.data))
        self.assertTrue(self.function.within_scale(self.values.data[0]))
        self.assertFalse(self.function.within_scale(150))
        self.assertTrue(
            GeneralizedCriterion(UShapeFunction()).within_scale(150)
        )

    def test_call(self):
        self.assertEqual(self.function(1, 0), 1)
        self.assertEqual(self.function(0, 1), 0)
        self.assertEqual(
            self.function(1, 0, preference_direction=PreferenceDirection.MIN),
            0,
        )
        assert_frame_equal(
            self.function(self.values.data), DataFrame([[0, 0], [1, 0]])
        )
        assert_frame_equal(
            self.function(
                self.values.data, preference_direction=PreferenceDirection.MIN
            ),
            DataFrame([[0, 1], [0, 0]]),
        )
        self.assertEqual(
            self.function(self.values), AdjacencyValueMatrix([[0, 0], [1, 0]])
        )
        with self.assertRaises(TypeError):
            self.function(0)


class TestGeneralizedCriteria(unittest.TestCase):
    def setUp(self):
        self.functions = {
            0: UShapeFunction(),
            1: UShapeFunction(2.5),
        }
        scales = {
            0: QuantitativeScale(
                Interval(0, 10), preference_direction=PreferenceDirection.MAX
            ),
            1: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MIN
            ),
        }
        self.table = PerformanceTable(
            [[0, 0], [1, 5]],
            scales=scales,
        )
        self.general_criteria = GeneralizedCriteria(
            {
                c: GeneralizedCriterion(self.functions[c], scales[c])
                for c in scales
            }
        )
        self.result = PartialValueMatrix(
            DataFrame(
                [
                    [Series([0, 0]), Series([0, 1])],
                    [Series([1, 0]), Series([0, 0])],
                ]
            )
        )

    def test_within_in_scales(self):
        self.assertTrue(self.general_criteria.within_in_scales(self.table))
        self.assertTrue(
            self.general_criteria.within_in_scales(self.table.data)
        )
        self.assertTrue(
            self.general_criteria.within_in_scales(
                self.table.alternatives_values[0]
            )
        )
        self.assertTrue(
            self.general_criteria.within_in_scales(
                self.table.alternatives_values[0].data
            )
        )
        self.assertFalse(
            self.general_criteria.within_in_scales(Series([-5, -5]))
        )

    def test_call(self):
        # result when no preference direction supplied
        other_result = PartialValueMatrix(
            DataFrame(
                [
                    [Series([0, 0]), Series([0, 0])],
                    [Series([1, 1]), Series([0, 0])],
                ]
            )
        )
        self.assertEqual(
            self.general_criteria(
                self.table.alternatives_values[0],
                self.table.alternatives_values[1],
            ),
            self.result.row_matrices[0].alternatives_values[1],
        )
        assert_series_equal(
            self.general_criteria(
                self.table.alternatives_values[0].data,
                self.table.alternatives_values[1].data,
            ),
            other_result.data.loc[0, 1],
        )
        assert_frame_equal(
            self.general_criteria(self.table.data),
            other_result.data,
        )
        self.assertEqual(self.general_criteria(self.table), self.result)
        with self.assertRaises(TypeError):
            self.general_criteria(Series([0, 1]))


def test_positive_flows():
    prefs1 = AdjacencyValueMatrix([[0, 1, 0], [1, 0, 1], [0, 0, 0]])
    expected = Series([1, 2, 0])
    assert_series_equal(positive_flows(prefs1), expected)
    expected2 = Series([0, 1])
    assert_series_equal(positive_flows(prefs1, profiles=[2]), expected2)


def test_negative_flows():
    prefs1 = AdjacencyValueMatrix([[0, 1, 0], [1, 0, 1], [0, 0, 0]])
    expected = Series([1, 1, 1])
    assert_series_equal(negative_flows(prefs1), expected)
    expected2 = Series([0, 0])
    assert_series_equal(negative_flows(prefs1, profiles=[2]), expected2)


def test_net_outranking_flows():
    prefs1 = AdjacencyValueMatrix([[0, 1, 0], [1, 0, 1], [0, 0, 0]])
    flows1 = Series([0, 1, -1])
    assert_series_equal(net_outranking_flows(prefs1), flows1)


def test_criteria_flows():
    partial_prefs = PartialValueMatrix(
        DataFrame(
            [
                [Series([0, 0]), Series([0, 1])],
                [Series([1, 0]), Series([0, 0])],
            ]
        )
    )
    expected = DataFrame([[-0.5, 0.5], [0.5, -0.5]])
    assert_frame_equal(criteria_flows(partial_prefs), expected)


class TestPromethee1(unittest.TestCase):
    def setUp(self):
        self.weights = {0: 0.5, 1: 3, 2: 1.5, 3: 0.2, 4: 2, 5: 1}
        self.functions = {
            0: UShapeFunction(),
            1: UShapeFunction(2.5),
            2: VShapeFunction(3),
            3: LevelFunction(1, 0.5),
            4: VShapeFunction(2, 1),
            5: GaussianFunction(1),
        }
        scales = {
            0: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
            1: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
            2: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
            3: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
            4: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
            5: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
        }
        self.table = PerformanceTable(
            [
                [1, 2, -1, 5, 2, 2],  # a1
                [3, 5, 3, -5, 3, 3],  # a2
                [3, -5, 3, 4, 3, 2],  # a3
                [2, -2, 2, 5, 1, 1],  # a4
                [3, 5, 3, -5, 3, 3],  # a5
            ],
            scales=scales,
        )
        self.partial_preferences = PartialValueMatrix(
            [
                [
                    Series([0, 0, 0, 0, 0, 0]),
                    Series([0, 0, 0, 1, 0, 0]),
                    Series([0.0, 1.0, 0.0, 0.5, 0.0, 0.0]),
                    Series([0.0, 1.0, 0.0, 0.0, 0.0, 0.3934693402873666]),
                    Series([0, 0, 0, 1, 0, 0]),
                ],
                [
                    Series([1.0, 1.0, 1.0, 0.0, 0.0, 0.3934693402873666]),
                    Series([0, 0, 0, 0, 0, 0]),
                    Series([0.0, 1.0, 0.0, 0.0, 0.0, 0.3934693402873666]),
                    Series(
                        [
                            1.0,
                            1.0,
                            0.3333333333333333,
                            0.0,
                            1.0,
                            0.8646647167633873,
                        ]
                    ),
                    Series([0, 0, 0, 0, 0, 0]),
                ],
                [
                    Series([1, 0, 1, 0, 0, 0]),
                    Series([0, 0, 0, 1, 0, 0]),
                    Series([0, 0, 0, 0, 0, 0]),
                    Series(
                        [
                            1.0,
                            0.0,
                            0.3333333333333333,
                            0.0,
                            1.0,
                            0.3934693402873666,
                        ]
                    ),
                    Series([0, 0, 0, 1, 0, 0]),
                ],
                [
                    Series([1.0, 0.0, 1.0, 0.0, 0.0, 0.0]),
                    Series([0, 0, 0, 1, 0, 0]),
                    Series([0.0, 1.0, 0.0, 0.5, 0.0, 0.0]),
                    Series([0, 0, 0, 0, 0, 0]),
                    Series([0, 0, 0, 1, 0, 0]),
                ],
                [
                    Series([1.0, 1.0, 1.0, 0.0, 0.0, 0.3934693402873666]),
                    Series([0, 0, 0, 0, 0, 0]),
                    Series([0.0, 1.0, 0.0, 0.0, 0.0, 0.3934693402873666]),
                    Series(
                        [
                            1.0,
                            1.0,
                            0.3333333333333333,
                            0.0,
                            1.0,
                            0.8646647167633873,
                        ]
                    ),
                    Series([0, 0, 0, 0, 0, 0]),
                ],
            ]
        )
        self.preferences = AdjacencyValueMatrix(
            [
                [
                    0.0,
                    0.02439024390243903,
                    0.37804878048780494,
                    0.41383772442528866,
                    0.02439024390243903,
                ],
                [
                    0.6577401634496789,
                    0.0,
                    0.41383772442528866,
                    0.8371542337516327,
                    0.0,
                ],
                [
                    0.24390243902439027,
                    0.02439024390243903,
                    0.0,
                    0.41383772442528866,
                    0.02439024390243903,
                ],
                [
                    0.24390243902439027,
                    0.02439024390243903,
                    0.37804878048780494,
                    0.0,
                    0.02439024390243903,
                ],
                [
                    0.6577401634496789,
                    0.0,
                    0.41383772442528866,
                    0.8371542337516327,
                    0.0,
                ],
            ]
        )
        self.flows = (
            Series(
                [
                    0.8406669927179717,
                    1.9087321216266002,
                    0.706520651254557,
                    0.6707317073170733,
                    1.9087321216266002,
                ]
            ),
            Series(
                [
                    1.8032852049481383,
                    0.07317073170731708,
                    1.5837730098261873,
                    2.5019839163538427,
                    0.07317073170731708,
                ]
            ),
        )
        self.promethee = Promethee1(self.table, self.weights, self.functions)

    def test_partial_preferences(self):
        assert_frame_equal(
            self.partial_preferences.data,
            self.promethee.partial_preferences().data,
            check_dtype=False,
        )

    def test_preferences(self):
        assert_frame_equal(
            self.promethee.preferences(self.partial_preferences).data,
            self.preferences.data,
        )

    def test_flows(self):
        pos_flows = self.promethee.flows(self.preferences)
        neg_flows = self.promethee.flows(self.preferences, negative=True)
        assert_series_equal(pos_flows, self.flows[0])
        assert_series_equal(neg_flows, self.flows[1])

    def test_rank(self):
        relations = self.promethee.rank()
        expected_relations = PreferenceStructure(
            [
                P(1, 0),
                R(0, 2),
                P(0, 3),
                P(4, 0),
                P(1, 2),
                P(1, 3),
                I(1, 4),
                P(2, 3),
                P(4, 2),
                P(4, 3),
            ]
        )
        self.assertEqual(relations, expected_relations)


class TestPromethee2(unittest.TestCase):
    def setUp(self):
        self.weights = {0: 0.5, 1: 3, 2: 1.5, 3: 0.2, 4: 2, 5: 1}
        self.functions = {
            0: UShapeFunction(),
            1: UShapeFunction(2.5),
            2: VShapeFunction(3),
            3: LevelFunction(1, 0.5),
            4: VShapeFunction(2, 1),
            5: GaussianFunction(1),
        }
        scales = {
            0: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
            1: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
            2: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
            3: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
            4: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
            5: QuantitativeScale(
                Interval(-5, 5), preference_direction=PreferenceDirection.MAX
            ),
        }
        self.table = PerformanceTable(
            [
                [1, 2, -1, 5, 2, 2],  # a1
                [3, 5, 3, -5, 3, 3],  # a2
                [3, -5, 3, 4, 3, 2],  # a3
                [2, -2, 2, 5, 1, 1],  # a4
                [3, 5, 3, -5, 3, 3],  # a5
            ],
            scales=scales,
        )
        self.partial_preferences = PartialValueMatrix(
            [
                [
                    Series([0, 0, 0, 0, 0, 0]),
                    Series([0, 0, 0, 1, 0, 0]),
                    Series([0.0, 1.0, 0.0, 0.5, 0.0, 0.0]),
                    Series([0.0, 1.0, 0.0, 0.0, 0.0, 0.3934693402873666]),
                    Series([0, 0, 0, 1, 0, 0]),
                ],
                [
                    Series([1.0, 1.0, 1.0, 0.0, 0.0, 0.3934693402873666]),
                    Series([0, 0, 0, 0, 0, 0]),
                    Series([0.0, 1.0, 0.0, 0.0, 0.0, 0.3934693402873666]),
                    Series(
                        [
                            1.0,
                            1.0,
                            0.3333333333333333,
                            0.0,
                            1.0,
                            0.8646647167633873,
                        ]
                    ),
                    Series([0, 0, 0, 0, 0, 0]),
                ],
                [
                    Series([1, 0, 1, 0, 0, 0]),
                    Series([0, 0, 0, 1, 0, 0]),
                    Series([0, 0, 0, 0, 0, 0]),
                    Series(
                        [
                            1.0,
                            0.0,
                            0.3333333333333333,
                            0.0,
                            1.0,
                            0.3934693402873666,
                        ]
                    ),
                    Series([0, 0, 0, 1, 0, 0]),
                ],
                [
                    Series([1.0, 0.0, 1.0, 0.0, 0.0, 0.0]),
                    Series([0, 0, 0, 1, 0, 0]),
                    Series([0.0, 1.0, 0.0, 0.5, 0.0, 0.0]),
                    Series([0, 0, 0, 0, 0, 0]),
                    Series([0, 0, 0, 1, 0, 0]),
                ],
                [
                    Series([1.0, 1.0, 1.0, 0.0, 0.0, 0.3934693402873666]),
                    Series([0, 0, 0, 0, 0, 0]),
                    Series([0.0, 1.0, 0.0, 0.0, 0.0, 0.3934693402873666]),
                    Series(
                        [
                            1.0,
                            1.0,
                            0.3333333333333333,
                            0.0,
                            1.0,
                            0.8646647167633873,
                        ]
                    ),
                    Series([0, 0, 0, 0, 0, 0]),
                ],
            ]
        )
        self.preferences = AdjacencyValueMatrix(
            [
                [
                    0.0,
                    0.02439024390243903,
                    0.37804878048780494,
                    0.41383772442528866,
                    0.02439024390243903,
                ],
                [
                    0.6577401634496789,
                    0.0,
                    0.41383772442528866,
                    0.8371542337516327,
                    0.0,
                ],
                [
                    0.24390243902439027,
                    0.02439024390243903,
                    0.0,
                    0.41383772442528866,
                    0.02439024390243903,
                ],
                [
                    0.24390243902439027,
                    0.02439024390243903,
                    0.37804878048780494,
                    0.0,
                    0.02439024390243903,
                ],
                [
                    0.6577401634496789,
                    0.0,
                    0.41383772442528866,
                    0.8371542337516327,
                    0.0,
                ],
            ]
        )
        self.flows = Series(
            [
                -0.9626182122301665,
                1.835561389919283,
                -0.8772523585716303,
                -1.8312522090367693,
                1.835561389919283,
            ]
        )
        self.promethee = Promethee2(self.table, self.weights, self.functions)

    def test_partial_preferences(self):
        assert_frame_equal(
            self.partial_preferences.data,
            self.promethee.partial_preferences().data,
            check_dtype=False,
        )

    def test_preferences(self):
        assert_frame_equal(
            self.promethee.preferences(self.partial_preferences).data,
            self.preferences.data,
        )

    def test_flows(self):
        assert_series_equal(self.promethee.flows(self.preferences), self.flows)

    def test_rank(self):
        res = self.promethee.rank()
        check_val = Values(
            Series([-0.96261, 1.83557, -0.87726, -1.83127, 1.83557])
        )
        assert_series_equal(res.data, check_val.data)


class TestPrometheeGaia(TestPromethee2):
    def setUp(self):
        super().setUp()
        self.promethee = PrometheeGaia(
            self.table, self.weights, self.functions
        )

    def test_unicriterion_net_flow_matrix(self):
        matrix = self.promethee.unicriterion_net_flows_matrix()
        dataset = (
            DataFrame(
                [
                    [-4, 0, -4.0, 2.5, 0.0, -0.393469],
                    [2, 3, 1.333333, -3.0, 1.0, 1.651603],
                    [2, -4, 1.333333, 1.0, 1.0, -0.393469],
                    [-2, -2, 1.110223e-16, 2.5, -3.0, -2.516268],
                    [2, 3, 1.333333, -3.0, 1.0, 1.651603],
                ]
            )
            / 5
        )
        assert_frame_equal(matrix, dataset, check_dtype=False)
