import unittest

from pandas import DataFrame, Series
from pandas.testing import assert_frame_equal, assert_series_equal

from mcda.functions import Interval
from mcda.matrices import PerformanceTable, create_outranking_matrix
from mcda.outranking.srmp import SRMP, ProfileWiseOutranking, SRMPLearner
from mcda.relations import I, P, PreferenceStructure
from mcda.scales import PreferenceDirection, QuantitativeScale
from mcda.values import CommensurableValues, Values


class TestSRMPLearner(unittest.TestCase):
    def setUp(self):
        self.scales = {
            0: QuantitativeScale(Interval(0, 1)),
            1: QuantitativeScale(Interval(3, 4)),
            2: QuantitativeScale(Interval(1, 2)),
            3: QuantitativeScale(Interval(0, 1)),
            4: QuantitativeScale(
                Interval(30, 100), preference_direction=PreferenceDirection.MIN
            ),
        }
        self.table = PerformanceTable(
            [
                [0.720, 3.560, 1.340, 0.62, 44.340],
                [0.8, 3.940, 1.430, 0.74, 36.360],
                [0.760, 3.630, 1.380, 0.89, 48.750],
                [0.780, 3.740, 1.450, 0.72, 42.130],
                [0.740, 3.540, 1.370, 0.73, 36.990],
                [0.690, 3.740, 1.450, 0.84, 42.430],
                [0.7, 3.280, 1.280, 0.83, 47.430],
                [0.860, 3.370, 1.150, 0.8, 80.790],
            ],
            scales=self.scales,
        )
        self.relations = PreferenceStructure(
            [
                P(1, 0),
                P(3, 2),
                P(3, 4),
                P(5, 4),
                P(5, 6),
                I(6, 7),
                I(7, 0),
                I(6, 0),
                P(1, 3),
                P(2, 6),
            ]
        )
        self.lexicographic_order = [1, 0]
        self.learner = SRMPLearner(
            self.table,
            self.relations,
            3,
            inconsistencies=True,
            gamma=0.1,
            non_dictator=True,
            solver_args=None,
        )

    def test_constructor(self):
        self.assertEqual(self.learner.performance_table, self.table)
        self.assertEqual(self.learner.relations, self.relations)
        self.assertEqual(self.learner.max_profiles_number, 3)
        self.assertIsNone(self.learner.profiles_number)
        self.assertIsNone(self.learner.lexicographic_order)
        self.assertEqual(self.learner.inconsistencies, True)
        self.assertEqual(self.learner.gamma, 0.1)
        self.assertEqual(self.learner.non_dictator, True)
        self.assertEqual(self.learner.solver_args, {})

        self.assertRaises(ValueError, SRMPLearner, self.table, self.relations)

    def test_learn_lexicographic_order(self):
        learner = SRMPLearner(
            self.table,
            self.relations,
            lexicographic_order=self.lexicographic_order,
            inconsistencies=True,
            gamma=0.1,
            non_dictator=True,
            solver_args=None,
        )
        srmp = learner.learn()
        prob = learner.problem
        fitness = learner.compute_fitness(prob, len(self.relations), True)
        # Check no dictator criterion exists
        for w in srmp.criteria_weights.values():
            self.assertTrue(w <= 0.5)
        self.assertEqual(type(srmp), SRMP)
        self.assertEqual(fitness, 0.9)
        self.assertEqual(srmp.lexicographic_order, self.lexicographic_order)
        self.assertEqual(
            srmp.criteria_weights, {0: 0.1, 1: 0.2, 2: 0.5, 3: 0.1, 4: 0.1}
        )
        assert_frame_equal(
            srmp.profiles.data,
            DataFrame(
                [
                    [0.00, 3.73, 1.55, 0.0, 99.36],
                    [0.69, 3.94, 2.00, 1.0, 80.79],
                ]
            ),
            atol=0.01,
        )

        learner = SRMPLearner(
            self.table,
            self.relations,
            lexicographic_order=self.lexicographic_order,
            inconsistencies=True,
            gamma=0.1,
            non_dictator=False,
            solver_args={},
        )
        srmp = learner.learn()
        prob = learner.problem
        self.assertEqual(
            srmp.criteria_weights, {0: 0.6, 1: 0.1, 2: 0.1, 3: 0.1, 4: 0.1}
        )

        learner = SRMPLearner(
            self.table,
            self.relations,
            lexicographic_order=self.lexicographic_order,
            inconsistencies=False,
            gamma=0.1,
            non_dictator=True,
            solver_args=None,
        )

        srmp = learner.learn()
        prob = learner.problem
        self.assertIsNone(srmp)
        self.assertEqual(
            learner.compute_fitness(prob, len(self.relations), False), 0
        )

    def test_learn(self):
        srmp = self.learner.learn()
        self.assertEqual(self.learner.fitness, 1.0)
        # Check no dictator criterion exists
        for w in srmp.criteria_weights.values():
            self.assertTrue(w <= 0.5)
        rank = srmp.rank()
        exp_rank = Series([4, 1, 3, 2, 3, 2, 4, 4])
        assert_series_equal(exp_rank, rank.data)

        learner = SRMPLearner(
            self.table,
            self.relations,
            profiles_number=3,
            inconsistencies=True,
            gamma=0.1,
            non_dictator=True,
            solver_args=None,
        )
        srmp2 = learner.learn()
        rank = srmp2.rank()
        assert_series_equal(exp_rank, rank.data)

        learner = SRMPLearner(
            self.table,
            self.relations,
            lexicographic_order=[0, 1, 2],
            inconsistencies=True,
            gamma=0.1,
            non_dictator=True,
            solver_args=None,
        )
        srmp3 = learner.learn()
        rank = srmp3.rank()
        assert_series_equal(exp_rank, rank.data)

        learner = SRMPLearner(
            self.table,
            self.relations,
            lexicographic_order=self.lexicographic_order,
            inconsistencies=False,
            gamma=0.1,
            non_dictator=True,
            solver_args=None,
        )
        srmp4 = learner.learn()
        self.assertIsNone(srmp4)

        learner = SRMPLearner(
            self.table,
            self.relations,
            max_profiles_number=2,
            inconsistencies=False,
            gamma=0.1,
            non_dictator=True,
            solver_args=None,
        )
        srmp5 = learner.learn()
        self.assertIsNone(srmp5)


class TestProfileWiseOutranking(unittest.TestCase):
    def setUp(self):
        self.scales = {
            0: QuantitativeScale(Interval(0, 1)),
            1: QuantitativeScale(Interval(3, 4)),
            2: QuantitativeScale(Interval(1, 2)),
            3: QuantitativeScale(Interval(0, 1)),
            4: QuantitativeScale(
                Interval(30, 100), preference_direction=PreferenceDirection.MIN
            ),
        }
        self.table = PerformanceTable(
            [
                [0.720, 3.560, 1.340, 0.62, 44.340],
                [0.8, 3.940, 1.430, 0.74, 36.360],
                [0.760, 3.630, 1.380, 0.89, 48.750],
                [0.780, 3.740, 1.450, 0.72, 42.130],
                [0.740, 3.540, 1.370, 0.73, 36.990],
                [0.690, 3.740, 1.450, 0.84, 42.430],
                [0.7, 3.280, 1.280, 0.83, 47.430],
                [0.860, 3.370, 1.150, 0.8, 80.790],
            ],
            scales=self.scales,
        )
        self.weights = {0: 30, 1: 30, 2: 20, 3: 10, 4: 10}
        self.profile = Values(
            [0.800, 3.700, 1.370, 0.790, 42.000],
            scales=self.scales,
        )
        self.ranker = ProfileWiseOutranking(
            self.table, self.weights, self.profile
        )
        self.outranking_matrix = create_outranking_matrix(
            [
                [1, 0, 0, 0, 0, 0, 0, 0],
                [1, 1, 1, 1, 1, 1, 1, 1],
                [1, 0, 1, 0, 1, 0, 1, 0],
                [1, 0, 1, 1, 1, 0, 1, 1],
                [1, 0, 1, 0, 1, 0, 1, 0],
                [1, 0, 1, 1, 1, 1, 1, 1],
                [1, 0, 0, 0, 0, 0, 1, 0],
                [1, 0, 1, 0, 1, 0, 1, 1],
            ]
        )

    def test_rank(self):
        self.assertEqual(self.ranker.rank(), self.outranking_matrix)


class TestSRMP(unittest.TestCase):
    def setUp(self):
        self.scales = {
            0: QuantitativeScale(Interval(0, 1)),
            1: QuantitativeScale(Interval(3, 4)),
            2: QuantitativeScale(Interval(1, 2)),
            3: QuantitativeScale(Interval(0, 1)),
            4: QuantitativeScale(
                Interval(30, 100), preference_direction=PreferenceDirection.MIN
            ),
        }
        self.table = PerformanceTable(
            [
                [0.720, 3.560, 1.340, 0.62, 44.340],
                [0.8, 3.940, 1.430, 0.74, 36.360],
                [0.760, 3.630, 1.380, 0.89, 48.750],
                [0.780, 3.740, 1.450, 0.72, 42.130],
                [0.740, 3.540, 1.370, 0.73, 36.990],
                [0.690, 3.740, 1.450, 0.84, 42.430],
                [0.7, 3.280, 1.280, 0.83, 47.430],
                [0.860, 3.370, 1.150, 0.8, 80.790],
            ],
            scales=self.scales,
        )
        self.weights = {0: 30, 1: 30, 2: 20, 3: 10, 4: 10}
        self.profiles = PerformanceTable(
            [
                [0.750, 3.500, 1.300, 0.730, 43.00],
                [0.800, 3.700, 1.370, 0.790, 42.000],
            ],
            scales=self.scales,
        )
        self.lexicographic_order = [1, 0]
        self.srmp = SRMP(
            self.table, self.weights, self.profiles, self.lexicographic_order
        )
        self.outranking_matrices = [
            create_outranking_matrix(
                [
                    [1, 0, 0, 0, 0, 0, 1, 1],
                    [1, 1, 1, 1, 1, 1, 1, 1],
                    [1, 0, 1, 1, 1, 1, 1, 1],
                    [1, 0, 1, 1, 1, 1, 1, 1],
                    [1, 0, 0, 0, 1, 1, 1, 1],
                    [1, 0, 0, 0, 1, 1, 1, 1],
                    [0, 0, 0, 0, 0, 0, 1, 0],
                    [0, 0, 0, 0, 0, 0, 1, 1],
                ]
            ),
            create_outranking_matrix(
                [
                    [1, 0, 0, 0, 0, 0, 0, 0],
                    [1, 1, 1, 1, 1, 1, 1, 1],
                    [1, 0, 1, 0, 1, 0, 1, 0],
                    [1, 0, 1, 1, 1, 0, 1, 1],
                    [1, 0, 1, 0, 1, 0, 1, 0],
                    [1, 0, 1, 1, 1, 1, 1, 1],
                    [1, 0, 0, 0, 0, 0, 1, 0],
                    [1, 0, 1, 0, 1, 0, 1, 1],
                ]
            ),
        ]
        self.rank = CommensurableValues(
            Series([8, 1, 5, 3, 6, 2, 7, 4]),
            scale=QuantitativeScale(
                Interval(1, 8), preference_direction=PreferenceDirection.MIN
            ),
        )

    def test_constructor(self):
        self.assertEqual(self.srmp.criteria_weights, self.weights)
        self.assertEqual(self.srmp.profiles, self.profiles)
        self.assertEqual(
            self.srmp.lexicographic_order, self.lexicographic_order
        )

    def test_sub_srmp(self):
        self.assertEqual(len(self.srmp.sub_srmp), 2)
        for p, sub_srmp in zip(
            self.profiles.alternatives_values.values(), self.srmp.sub_srmp
        ):
            self.assertEqual(sub_srmp.profile, p)
            self.assertEqual(sub_srmp.performance_table, self.table)
            self.assertEqual(sub_srmp.criteria_weights, self.weights)

    def test_construct(self):
        self.assertEqual(self.outranking_matrices, self.srmp.construct())

    def test_exploit(self):
        assert_series_equal(
            self.rank.data, self.srmp.exploit(self.outranking_matrices).data
        )

    def test_rank(self):
        assert_series_equal(self.rank.data, self.srmp.rank().data)

    def test_learn(self):
        """
        .. todo:: replace SRMPTrainer.learn subcall by a mockup patch
        """
        relations = PreferenceStructure(
            [
                P(1, 0),
                P(3, 2),
                P(3, 4),
                P(5, 4),
                P(5, 6),
                I(6, 7),
                I(7, 0),
                I(6, 0),
                P(1, 3),
                P(2, 6),
            ]
        )
        srmp = SRMP.learn(
            self.table,
            relations,
            max_profiles_number=3,
            inconsistencies=True,
            gamma=0.1,
            non_dictator=True,
            solver_args=None,
        )

        # Check no dictator criterion exists
        for w in srmp.criteria_weights.values():
            self.assertTrue(w <= 0.5)
        rank = srmp.rank()
        exp_rank = Series([4, 1, 3, 2, 3, 2, 4, 4])
        assert_series_equal(exp_rank, rank.data)
