#!/bin/bash

current_version() {
    echo "$(git describe --tags --match "v[0-9]*")"
}

latest_major() {
    PART=$(echo $1 | grep -m 1 -Eo 'v[0-9]+')
    echo "${PART}.0.0"
}

latest_minor() {
    PART=$(echo $1 | grep -m 1 -Eo 'v[0-9]+.[0-9]+')
    echo "${PART}.0"
}

latest_patch() {
    PART=$(echo $1 | grep -m 1 -Eo 'v[0-9]+.[0-9]+.[0-9]+')
    echo "${PART}"
}

export CURRENT=$(current_version)
export MAJOR=$(latest_major $CURRENT)
export MINOR=$(latest_minor $CURRENT)
export PATCH=$(latest_patch $CURRENT)