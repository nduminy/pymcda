Policies
========

Version policy
--------------

mcda uses semantic versioning to decide of its deprecation policy, API
back-compatibility and version numbering.

A mcda release number is formatted: ``MAJOR.MINOR.PATCH``

API breaking changes must only occur in **major** releases. They must be
documented in the doc-strings and announced whenever they are first
thought of, but must only be enforced in a **major** release. If
deprecation message doesn't precise at which version the deprecation
will be enforced, it is assumed to be the next **major** release. (any
deprecation must at least indicate at which version it was introduced)

**Minor** releases will introduce new features, and deprecations.

**Patch** releases must only consist of small internal changes and bug
fixes. Any changes that don't introduce a change in the API or the
package behaviour.

**Note: behavioural changes may be made on minor or patches if the
previous behaviour vas clearly not intended. (for example a fringe case
in an algorithmic result that don't conform to its implementation
reference)**

See our section about :ref:`backward compatibility <contributing/contributions:Backward compatibility>`
if you need more details. You can find deprection recipes :ref:`here <contributing/guides:Deprecation recipes>`.

Python support
--------------

This package currently supports all released and maintained python
versions (see https://devguide.python.org/versions/).

This corresponds to all versions of python>=3.8.

We may change our policy towards Numpy recommendations in the future
(see `NEP
29 <https://numpy.org/neps/nep-0029-deprecation_policy.html>`__)

Python version subsequent changes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here is the list of identified changes and improvements that will be unlocked after dropping support of older python versions:

* 3.10+

  * All `Union` and `Optional` types can be replaced by simplier `|` symbol (see `PEP-604 <https://peps.python.org/pep-0604/>`__)

* 3.11+

  * `typing_extensions` module can be removed and `Self` imported systematically from `typing` (See `PEP-673 <https://peps.python.org/pep-0673/>`__)
