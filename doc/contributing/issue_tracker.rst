How to interact with the issue tracker?
=======================================

One good way to contribute to this package, is by creating or
interacting with issues.

This package is hosted on
`gitlab <https://gitlab.com/decide.imt-atlantique/pymcda>`__, it uses
the provided `issue
tracker <https://gitlab.com/decide.imt-atlantique/pymcda/-/issues>`__.
Anyone authorized can open a new issue. When opening an issue, there is
a form to fill, detailing the issue. An issue can be about one of the
following:

-  a broken feature, bug encountered
-  a feature not working as expected or intended
-  a feature improvement
-  a new feature suggestion

Properly describe the issue
---------------------------

It is important to describe the issue as clearly as possible:

-  The subject must be both clear and concise
-  The issue detailed description **must** contain the commit checksum
   or the :ref:`version number <contributing/repository:Tags>` of the code used when getting the
   issue
-  If the issue is about a bug, the detailed description should also
   contain all the information necessary to reproduce the bug (script
   launched, data used)

Analyze the issue
-----------------

Not all issues require a quick development to solve, or even any
development at all. However, they all need to be analyzed and answered
quickly.

The first task for a manager or a developer is to analyze the issue, its
urgency and what it would entail. Generally the **highest** priority is
on the bugs/broken feature on the ``master`` branch, then on bugs/broken
feature on the ``dev`` branch, finally on the rest. Based on that, the
manager or a developer should decide when this issue will start being
worked on, and when it should be resolved (if it must be worked on at
all), or at least the conditions needed before resolving it (development
of another feature for example, release of a new version, etc). Then an
answer should be posted on the issue page to clearly state this
analysis.

If it is decided this issue will not be resolved in the near future, it
should be indicated.

If it is decided this issue won't be resolved or is completely
irrelevant, it should be indicated too, and the issue should be closed.
If not, a developer should be assigned with its resolution.

Assign the issue
----------------

If you are a contributor or even the main contributor managing the
online git repository, you can assign the issue to a developer which is
then tasked with resolving it. It is important to assign one issue to
only one developer, to avoid unnecessary conflicts very probable when
working on the same pieces of code at the same time.

Then the developer can interact with the person posting the issue.

The assigned developer will start by creating a temporary branch,
dedicated to the work on this issue (see :doc:`this <repository>`).

Note: if the issue originates from the ``master`` branch, and it
prevents the good usage of the code, its resolution is known as a
hotfix, and the changes made to resolve it will be brought on the
``master`` branch **as soon as possible**. Feature extensions or
suggestions on the other hand do not require a fast merge on ``master``
and can be integrated in the regular development cycle on the ``dev``
branch.

The developer **must** then reference the issue in any commits made
related to it (see :ref:`version number <contributing/repository:Commits>`).

Close the issue
---------------

Depending on the project management organization, either the assigned
developer or a manager reviewing the process, will close the issue once
resolved.

Before closing the issue or declaring it resolved, the developer will
merge the temporary branch on the branch where the issue originated (e.g
``dev`` if issue on ``dev``, ``dev`` and ``master`` if hotfix on
``master``).

In both cases, the assigned developer must indicates clearly by a post
on the issue when the issue is resolved, along with the commit checksum
or :ref:`version number <contributing/repository:Tags>` of the merge commit that solved it.

Alternatively, the assigned developer can mention in the message of the
last commit before merging on the common branches that it fixes the
issue. To do that, simply put ``Fix #n`` or ``Close #n`` on the last
line of the commit message (with ``n`` the issue number). Then when
creating and applying the merge request, the issue will be closed
automatically.

