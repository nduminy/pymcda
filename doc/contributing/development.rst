How to contribute?
==================

Setup environment
-----------------

If you want to start contributing to this project, you will need to set
up your development environment. The project uses python **3.10.7**. We
recommend installing it using a dev container or a :ref:`python
environment <contributing/guides:Python environments>` (see step 2).

1. First start by forking this repository. Go to our `Gitlab
   page <https://gitlab.com/decide.imt-atlantique/pymcda.git>`__ and hit
   the ``fork`` button. Then you need to clone your fork on your
   machine:

   .. code:: console

      $ git clone https://your.gitlab.website/your.user.name/mcda.git pymcda
      $ cd pymcda
      $ git remote add upstream https://gitlab.com/decide.imt-atlantique/pymcda.git

2. Then **we recommend that you create a separate python environment for
   this project** using for example *pyenv and pyenv-virtualenv* (see :ref:`python environment <contributing/guides:Python environments>`
   for installation details)

   .. code:: console

      $ pyenv install 3.10.7          # Install python 3.10.7 with pyenv
      $ pyenv virtualenv 3.10.7 mcda  # Create a python 3.10.7 virtualenv called 'mcda'
      $ pyenv local mcda              # Set up 'community-analysis' as the environment to use in the project folder


3. You can then install the system requirements:

   .. code:: console

      $ sudo apt-get install graphviz pandoc

4. You can then install the required python packages:

   .. code:: console

      $ make install

   .. note::
      This command uses ``pip``, if you didn't follow step 2, you
      need to have it installed. Otherwise you can use the project
      ``requirements.txt`` file and install them the way you want.

5. **We strongly recommend you set up a pre-commit hook** so that you
   can test your changes locally automatically when commiting (see
   :doc:`this <repository>`)

Alternatively, you could replace steps 2 through 4 by using our VS Code
devcontainer. You need the `docker
engine <https://docs.docker.com/engine/install/>`__ installed for that.

1. Install the
   `requirements <https://code.visualstudio.com/docs/devcontainers/containers>`__
   to use dev containers inside VS Code
2. Simply open the project folder in VS Code
3. Click the **green button** located in the bottom-left corner of VS
   Code (or open the command palette)
4. Select **Remote Containers - Reopen in Container**
5. VS Code will now reload and start initializing the containers. Wait
   for it to complete. This may take a while the very first time as pip
   dependencies must be installed.

We provide a `pre-commit hook <https://gitlab.com/decide.imt-atlantique/pymcda/-/blob/master/pre-commit>`__ that will test your code
when commiting. It tests the :ref:`unit tests <contributing/contributions:Unit testing>` and the
:ref:`linters <contributing/contributions:Linters>` and cancels the commit if they fail. You can set
it up easily:

.. code:: console

   $ chmod +x pre-commit         # make sure it has execution rights
   $ cp pre-commit .git/hooks/.  # install pre-commit hook

You are ready to go! If you are unsure, you can run some of the tests
included in the :ref:`Makefile <contributing/repository:Makefile>`.

Development
-----------

As mentionned earlier, the ``dev`` branch is the most up-to-date version
of the project. You should always start from this branch when developing
a new feature or bugfix. And any change made shall ultimately be merged
on this branch.

Identify an issue
~~~~~~~~~~~~~~~~~

It is a good practice to keep an issue and bug tracker as complete and
up-to-date as possible (see :doc:`issue tracker <issue_tracker>`).

So it is recommended you start any development by creating a new issue,
or selecting an open issue on the gitlab repository. You can then assign
this issue to yourself so other contributors know this issue is being
taken care of.

Create temporary branch
~~~~~~~~~~~~~~~~~~~~~~~

You should create a branch specific to the development you want to do,
this branch shall spans from the up-to-date ``dev`` branch:

.. code:: console

   $ git checkout dev              # get on 'dev' branch
   $ git pull                      # update branch so it matches its remote state
   $ git checkout -b n-xxxx        # create and get on a feature branch (when resolving an issue 'xxxx' with number 'n')

Alternatively, you can go on Gitlab, select the issue you are working
on, and click on the drop-down part of the blue button that says “create
merge request” and select “create branch” instead. Make sure the new
branch spans from ``dev`` (for a feature or bugfix) or ``master`` (only
for a hotfix). Then you can work on this branch:

.. code:: console

   $ git fetch                     # fetch all changes on the remote repository
   $ git checkout n-xxxx           # get the temporary branch (replace its name with the one you chose on gitlab)

Working on temporary branch
~~~~~~~~~~~~~~~~~~~~~~~~~~~

When working, remember to commit and push your changes often. **If
working on a specified issue, always reference that issue in the commit
messages** (see :ref:`this <contributing/repository:Commits>`). **Also whenever an issue has been
resolved, mention it in the message of the commit solving the issue.**
We recommend that you develop alongside the followings:

-  source code
-  documentation (see :ref:`this <contributing/contributions:Code documentation>` for more details)

   -  doc-string (see :ref:`this <contributing/contributions:Doc strings>` for more details)
   -  type hints (see :ref:`this <contributing/contributions:Type hinting>` for more details)
   -  comments (see :ref:`this <contributing/contributions:Comments>` for more details)

-  unit tests (see :ref:`this <contributing/contributions:Unit testing>` for more details)
-  notebooks showing how a high-level use of your newly added features
   (see :ref:`this <contributing/contributions:Notebooks>` for more details)

We also recommend you test your code as often as possible, using the
provided :ref:`Makefile <contributing/repository:Makefile>` targets.

You can keep up with other branches progress, using the following
command:

.. code:: console

   $ git fetch

Merging branch on common dev branch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When your development is over and you think it can be merged back into
the ``dev`` branch, you shall check the following:

1. Your changes actually accomplish what they were intended (e.g the
   issue related to the branch is resolved)
2. Your code passes the :ref:`linters <contributing/contributions:Linters>`
3. The :ref:`documentation <contributing/contributions:Code documentation>` of your changes is
   complete
4. The :ref:`unit tests <contributing/contributions:Unit testing>` are working and
   :ref:`cover <contributing/contributions:Coverage testing>` all your changes (*exception tolerated
   for plotting functions*)
5. The :ref:`notebooks <contributing/contributions:Notebooks>` cover your changes (recommended but
   not mandatory)
6. The changes have been appended to the top of the
   :ref:`changelog <contributing/repository:Changelog>`
7. The package requirements are up-to-date (both in the
   ``requirements.txt`` and ``setup.py`` files)
8. All your changes have been pushed on the online repository

Then you can open a new merge request on gitlab, requesting the merge of
your branch on our ``dev`` branch. Please use the following options:

-  Delete source branch after merge
-  **Do not stash changes before merge**

Then your merge request will be checked and only applied when approved
by a project maintainer. While the merge request is open, you can add
commits to it, and interact with other developers about the request.
Until the merge request is approved and applied, you may have to come
back to it and perform some changes.

However in the meantime, you can start working on a new feature.

We recommend to start from the ``dev`` branch, however in the case where
the previous merge request has not yet been accepted and your new
feature will depend on it, you may start a new branch spanning from your
not-yet-merged branch.

When the merge is applied, you can delete your local branch and its
remote tracking:

.. code:: console

   $ git branch -d n-xxxx
   $ git branch -d -r origin/n-xxxx

Release a new version
~~~~~~~~~~~~~~~~~~~~~

When enough features have been added, or if a package-breaking bug has
been fixed, we may want to release a new version of the package on
`PyPI <https://pypi.org/project/mcda/>`__. We will check the following:

1. Your code passes the :ref:`linters <contributing/contributions:Linters>`
2. The :ref:`documentation <contributing/contributions:Code documentation>` of your changes is
   complete
3. The :ref:`unit tests <contributing/contributions:Unit testing>` are working and
   :ref:`cover <contributing/contributions:Coverage testing>` all your changes (*exception tolerated
   for plotting functions*)
4. The :ref:`notebooks <contributing/contributions:Notebooks>` cover your changes (recommended but
   not mandatory)
5. The changes have been appended to the top of the
   :ref:`changelog <contributing/repository:Changelog>`
6. A version number is associated to the release and appended at the top
   of the :ref:`changelog <contributing/repository:Changelog>`
7. The package requirements are up-to-date (both in the
   ``requirements.txt`` and ``setup.py`` files)

Then we can open a new merge request on gitlab, requesting the merge of
the ``dev`` on the ``master`` branch. Please use the following options:

-  Delete source branch after merge
-  **Do not stash changes before merge**

Then our merge request will be checked and only applied when approved by
a project maintainer. While the merge request is open, we can add
commits to it, and interact with other developers about the request.
Until the merge request is approved and applied, we may have to come
back to it and perform some changes.

When the merge request is approved and applied, the gitlab pipeline for
the ``master`` branch will include a new extra job to deploy the new
version of the package on `PyPI <https://pypi.org/project/mcda/>`__
which is triggered manually.

Then to ensure the correspondence between the release on gitlab and
PyPI, we need to tag the commit of the release:

.. code:: console

   $ git checkout master
   $ git pull
   $ git tag v{a}.{b}.{c}
   $ git push --tags

The tag can also be applied from the gitlab web page.

