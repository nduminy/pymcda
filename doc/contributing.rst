Contributing
============

Contributions to this package can come in the form of issues (feature
requests, bugs, etc.) but also in the form of direct code contribution.


.. toctree::

  contributing/issue_tracker
  contributing/development
  contributing/contributions
  contributing/repository
  contributing/policies
  contributing/guides
  contributing/complete_example