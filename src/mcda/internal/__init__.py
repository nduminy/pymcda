"""
This subpackage contains all internal functionalities.
This is not considered part of the public API of this package.
Its modules and features are only intended for contributors and may encounter
many changes.
"""
