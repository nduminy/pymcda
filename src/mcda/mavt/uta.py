"""This module implements the UTA algorithms.

Implementation and naming conventions are taken from :cite:p:`siskos2005uta`.
"""
from ..internal.mavt.uta import UTA, UTAstar

__all__ = ["UTA", "UTAstar"]
